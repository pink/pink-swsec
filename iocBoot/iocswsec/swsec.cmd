#!../../bin/linux-x86_64/swsec

## You may have to change swsec to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/swsec.dbd"
swsec_registerRecordDeviceDriver pdbbase

epicsEnvSet ("IOCBL", "PINK")
epicsEnvSet ("IOCDEV", "SWSEC")

## Load record instances
dbLoadRecords("$(EXSUB)/db/exsub.db","P=$(IOCBL):$(IOCDEV):,R=exsub")

cd "${TOP}/iocBoot/${IOC}"

dbLoadRecords("swsec.db","BL=$(IOCBL),DEV=$(IOCDEV)")
dbLoadTemplate("swsec.substitutions", "BL=$(IOCBL),DEV=$(IOCDEV)")

set_savefile_path("/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=$(IOCBL),DEV=$(IOCDEV)")

## Start any sequence programs
#seq sncxxx,"user=epics"
