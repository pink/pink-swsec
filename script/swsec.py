#!/usr/bin/python3

import numpy as np
import epics
import queue
import time

#global variables
loglist = []

class COND:
    def __init__(self,N):
        self.pvname_enable = "PINK:SWSEC:{:d}:enable".format(int(N))
        self.pvname_state = "PINK:SWSEC:{:d}:state".format(int(N))
        self.pvname_value1 = "PINK:SWSEC:{:d}:val1".format(int(N))
        self.pvname_value2 = "PINK:SWSEC:{:d}:val2".format(int(N))
        self.enable = epics.PV(self.pvname_enable, auto_monitor=True)
        self.state = epics.PV(self.pvname_state, auto_monitor=True)
        self.val1 = epics.PV(self.pvname_value1, auto_monitor=True)
        self.val2 = epics.PV(self.pvname_value2, auto_monitor=True)

def logupdate():
    global loglist
    nlines = 30
    loglist = loglist[-nlines:]
    revlist = loglist.copy()
    revlist.reverse()
    logstr = '\n'.join(revlist)
    pv_log.put(logstr)

def logpush(msg):
    global loglist
    outmsg = "[{}] ".format(time.asctime())+msg
    loglist.append(outmsg)

## create PV channels - essential
print("Connecting essential PVs...")
pv_log = epics.PV("PINK:SWSEC:log", auto_monitor=False)
pv_exsub = epics.PV("PINK:SWSEC:exsub_py", auto_monitor=True)

## Watchdog PV
pv_wd_alive = epics.PV("PINK:PKWD:1:alive", auto_monitor=False)

## create PV channels - SWSEC channels
print("Connecting swsec PVs...")
num_conditions = 10
condition=[]
for i in range(num_conditions):
    condition.append(COND(int(i+1)))

## create PV channels - monitor
print("Connecting extra PVs...")
G11 = epics.PV("PINK:MAXB:S2Measure", auto_monitor=True)
G04 = epics.PV("PINK:MAXB:S3Measure", auto_monitor=True)

## wait for PV connections and empty queue
print("Waiting for PV connections...")
time.sleep(2)
pv_exsub.put(0)
time.sleep(1)

## main loop
print("SWSEC script running ...")
logpush("SWSEC script started")

while(pv_exsub.value==0):
    try:
        ## cond 1
        cond = condition[0]
        v1 = cond.val1.value
        v2 = cond.val2.value
        state = int(cond.state.value)
        if (G11.value>=v1):
            if state==0:
                cond.state.put(1)
                if cond.enable.value:
                    #action
                    msg = "CCD cooling will be turned off. G11 is too high"
                    logpush(msg)
                    epics.caput("PINK:GEYES:cam1:GreatEyesEnableCooling", 0, timeout=2)
        else:
            if state==1:
                cond.state.put(0)
                if cond.enable.value:
                    msg = "Condition 1 rearmed"
                    logpush(msg)

        ## cond 2
        cond = condition[1]
        v1 = cond.val1.value
        v2 = cond.val2.value
        state = int(cond.state.value)
        if (G04.value>=v1):
            if state==0:
                cond.state.put(1)
                if cond.enable.value:
                    #action
                    msg = "G04 too high. Closing V35"
                    logpush(msg)
                    epics.caput("PINK:PLCVAC:V35close", 1, timeout=2)
        else:
            if state==1:
                cond.state.put(0)
                if cond.enable.value:
                    msg = "Condition 2 rearmed"
                    logpush(msg)

        ## cond 3
        cond = condition[2]
        v1 = cond.val1.value
        v2 = cond.val2.value
        state = int(cond.state.value)
        if (G11.value>=v1 or G04.value>=v2):
            if state==0:
                cond.state.put(1)
                if cond.enable.value:
                    #action
                    msg = "G11 or G04 too high. Closing V19"
                    logpush(msg)
                    epics.caput("PINK:PLCGAS:V19close", 1, timeout=2)
        else:
            if state==1:
                cond.state.put(0)
                if cond.enable.value:
                    msg = "Condition 3 rearmed"
                    logpush(msg)

        # Watchdog update
        pv_wd_alive.put(1)

    except Exception as err:
        logpush(str(err))

    # Update log
    logupdate()
    time.sleep(1)

## Script exit
msg = "Script has stopped"
logpush(msg)
logupdate()
time.sleep(5)
print("OK")

